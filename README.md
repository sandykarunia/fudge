# Fudge

[![coverage report](https://gitlab.com/sandykarunia/fudge/badges/master/coverage.svg)](https://gitlab.com/sandykarunia/fudge/commits/master)
[![pipeline status](https://gitlab.com/sandykarunia/fudge/badges/master/pipeline.svg)](https://gitlab.com/sandykarunia/fudge/commits/master)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Fudge is an easy-to-use, easy-to-integrate judge system that runs on Linux machine. 