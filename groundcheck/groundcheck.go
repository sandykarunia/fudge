package groundcheck

import (
	"errors"
	"gitlab.com/sandykarunia/fudge/groundcheck/checkers"
)

var (
	errCheckAllFailed = errors.New("groundcheck: at least one check failed")
)

// GroundCheck is an entity that checks the machines where the program will run
//go:generate mockery -name=GroundCheck
type GroundCheck interface {
	// CheckAll observes the environment / all necessities to run fudge program
	CheckAll() error
}

type groundCheckImpl struct {
	c checkers.Checkers
}

func (g *groundCheckImpl) CheckAll() error {
	var errRes error

	var checkerFuncs = []func() bool{
		g.c.CheckSudo,
		g.c.CheckLibcapDevPkg,
		g.c.CheckIsolateBinaryExists,
		g.c.CheckIsolateBinaryExecutable,
	}

	// we don't want to interrupt the checks (i.e. put return inside the loop)
	// because we want the loop to keep going, as the functions will provide nice messages

	for _, f := range checkerFuncs {
		if !f() {
			errRes = errCheckAllFailed
		}
	}

	return errRes
}
